$fn = 360;

d = 60;
D = 100;
bolts = 3;
bolt_diameter = 4;
pins = bolts;
pin_diameter = bolt_diameter;
s = 1;
r = (d + D) / 4;

linear_extrude(s){
    difference(){
        circle(d = D);
        circle(d = d);

        for (i=[0 : bolts - 1]) {
            rotate([0, 0, i * (360 / bolts)]) {
                translate([r, 0, 0]) {
                    circle(d = bolt_diameter);
                }
            }
        }

        for (i=[0 : pins - 1]) {
            rotate([0, 0, i * (360 / bolts) + (180 / bolts)]) {
                translate([r, 0, 0]) {
                    circle(d = pin_diameter);
                }
            }
        }
    }
}
