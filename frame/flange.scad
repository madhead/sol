/*
    d                Flange's inner diameter (aperture), millimeters.
    D                Flange's outer diameter, millimeters.
    s                Flange's material thickness, millimeters.
    bolts            Number of flange bolts.
    bolt_diameter    Bolt hole diameter, millimeters.
    film_overlap     Solar film aperture overlap, millimeters. 
*/
module flange(d, D, s = 1, bolts = 3, bolt_diameter = 4, film_overlap = 5) {
    linear_extrude(height = 2 * s, center = true) {
        difference() {
            circle(d = D);
            circle(d = d);

            r = (d + D) / 4;

            for (i=[0 : bolts - 1]) {
                rotate([0, 0, i * (360 / bolts)]) {
                    translate([r, 0, 0]) {
                        circle(d = bolt_diameter);
                    }
                }
            }
        };
    }

    % linear_extrude(height = s/100, center = true) {
        circle(d = d + 2 * film_overlap);
    }
}
