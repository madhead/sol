$fn = 360;

d = 60;
D = 100;
bolts = 3;
bolt_diameter = 4;
s = 1;
r = (d + D) / 4;
hood_aperture = 53;
hood_thickness = 10;

difference() {
    hull() {
        linear_extrude(s){
            circle(d = hood_aperture + hood_thickness * 2);

            for (i=[0 : bolts - 1]) {
                rotate([0, 0, i * (360 / bolts)]) {
                    translate([r, 0, 0]) {
                        circle(d = (D - d) / 2);
                    }
                }
            }
        }
    }

    translate([0, 0, -s]){
        # cylinder(r = hood_aperture / 2, h = 3 * s);
    }

    for (i=[0 : bolts - 1]) {
        rotate([0, 0, i * (360 / bolts)]) {
            translate([r, 0, -s]) {
                # cylinder(r = bolt_diameter / 2, h = 3 * s);
            }
        }
    }
}