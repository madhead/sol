/*
    http://www.mennon-usa.com/products/lens-hoods/mennon-dv-s-58-screw-mount-58mm-digital-video-camcorder-lens-hood-with-cap-black.html
*/
module mennon_dvs_58() {
    $fn = 100;

    color([0.2, 0.2, 0.2]) union() {
        intersection() {
            linear_extrude(height = 40){
                polygon(
                    points = [
                        [-25.000, -37.000],
                        [25.000, -37.000],
                        [40.500, -21.500],
                        [40.500, 21.500],
                        [25.000, 37.000],
                        [-25.000, 37.000],
                        [-40.500, 21.500],
                        [-40.500, -21.500]
                    ]
                );
            };

            cylinder(h = 40, r1 = 60, r2 = 66 / 2, center = false);
        };

        translate([0, 0, 40]) cylinder(h = 7, r = 58/2);
    }
}

mennon_dvs_58();